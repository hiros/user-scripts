// ==UserScript==
// @name         bockusBackgrounds
// @namespace    http://tampermonkey.net/
// @version      0.1
// @author       Me
// @match        www.cosc.brocku.ca/~bockusd/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Your code here...
    document.body.style.background = "#ffffff";
})();