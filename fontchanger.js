// ==UserScript==
// @name         Change Font To Source Code Pro
// @namespace    http://tampermonkey.net/
// @version      0.4
// @updateURL    https://gitlab.com/hiros/user-scripts/raw/master/fontchanger.js
// @author       Me
// @match        *://*/*
// @grant        none
// @run-at document-end
// ==/UserScript==

(function() {
    'use strict';

    // Your code here...
    var style = document.createElement('style');
    style.innerHTML = `@import url('https://fonts.googleapis.com/css?family=Source+Code+Pro');\n* { font-family: source code pro !important; }`;
    document.head.appendChild(style);
})();